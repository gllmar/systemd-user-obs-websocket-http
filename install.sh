SERVICE_NAME=obs-websocket-http-server
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo "====> cloning dependencies"
git clone https://github.com/IRLToolkit/obs-websocket-http
cd "$DIR"/obs-websocket-http
echo "====>  pulling updates"
git pull

echo "====> installing requirements "
sudo python -m pip install -r requirements.txt

echo "====> overwriting default config file"
cp "$DIR"/config.ini "$DIR"/obs-websocket-http/config.ini

echo "====> creating executable "
echo '#!/bin/bash' > "$DIR"/obs-websocket-http-server 
echo  "cd $DIR/obs-websocket-http/"  >> "$DIR"/obs-websocket-http-server 
echo "/usr/bin/python ./main.py"	>> "$DIR"/obs-websocket-http-server 

chmod +x "$DIR"/obs-websocket-http-server

echo "====> copying to /usr/local/bin " 
sudo cp  "$DIR"/obs-websocket-http-server /usr/local/bin/obs-websocket-http-server

echo "====> copying service to user service folder"

mkdir -p ~/.config/systemd/user/
cp "$DIR"/$SERVICE_NAME.service ~/.config/systemd/user/$SERVICE_NAME.service
echo "====> reloading systemd daemon"
systemctl --user daemon-reload
