# systemd-user-obs-websocket-http

provide obs-websocket-http-server.service

## example 
```
curl -XPOST -H "Content-type: application/json" -d '{"scene-name":"Scene"}' 'http://127.0.0.1:4445/call/SetPreviewScene'
curl -XPOST -H "Content-type: application/json" -d '{"scene-name":"player"}' 'http://127.0.0.1:4445/call/SetPreviewScene'
curl -XPOST -H "Content-type: application/json" -d '{"type":"StudioProgram"}' 'http://127.0.0.1:4445/call/OpenProjector'
curl -XPOST -H "Content-type: application/json" -d '{"type":"StudioProgram","monitor":0}' 'http://127.0.0.1:4445/call/OpenProjector'
```
